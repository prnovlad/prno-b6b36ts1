package cz.cvut.fel.ts1.shop;

import cz.cvut.fel.ts1.storage.NoItemInStorage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

public class EShopContollerTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;
    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }
    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
    @BeforeEach
    void setUp() {
        EShopController.resetEShop();
        EShopController.startEShop();

        /* make up an artificial data */

        int[] itemCount = {10,1,4,5,10,2};


        Item[] storageItems = {
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };

        // insert data to the storage
        for (int i = 0; i < storageItems.length; i++) {
            EShopController.getStorage().insertItems(storageItems[i], itemCount[i]);
        }
    }

    @Test
    void createCart_FillCart_RemoveItem_Purchase_Process() throws NoItemInStorage {
        // create new cart
        ShoppingCart cart = EShopController.newCart();
        assertTrue(cart.getCartItems().isEmpty());

        // add item to the cart
        cart.addItem(EShopController.getStorage().getItem(1));
        assertEquals(1, cart.getItemsCount());
        assertEquals("Dancing Panda v.2", cart.getCartItems().get(0).getName());

        // add more items to the cart
        cart.addItem(EShopController.getStorage().getItem(3));
        cart.addItem(EShopController.getStorage().getItem(3));
        cart.addItem(EShopController.getStorage().getItem(3));
        assertEquals(4, cart.getItemsCount());

        // remove item from the cart
        cart.removeItem(1);
        assertEquals(3, cart.getCartItems().size());

        // purchase the cart
        EShopController.purchaseShoppingCart(cart, "Marek", "Hlavna 1");
        assertEquals(0, EShopController.getPurchasesArchive().getHowManyTimesHasBeenItemSold(EShopController.getStorage().getItem(1)));
        assertEquals(3, EShopController.getPurchasesArchive().getHowManyTimesHasBeenItemSold(EShopController.getStorage().getItem(3)));
        assertEquals(10, EShopController.getStorage().getItemCount(1));
        assertEquals(1, EShopController.getStorage().getItemCount(3));
    }

    // Error only in line separators
    @Test
    void Purchase_EmptyCart_FillCart_BuyMoreThanInStorage_Process() throws NoItemInStorage {
        // create new cart
        ShoppingCart cart = EShopController.newCart();
        assertTrue(cart.getCartItems().isEmpty());

        // purchase the cart
        EShopController.purchaseShoppingCart(cart, "Marek", "Hlavna 1");
        assertEquals("Error: shopping cart is empty" + System.lineSeparator(), outContent.toString());

        // add item to the cart
        cart.addItem(EShopController.getStorage().getItem(2));
        assertEquals(1, cart.getItemsCount());
        assertEquals("Dancing Panda v.3 with USB port", cart.getCartItems().get(0).getName());

        // add more items to the cart
        cart.addItem(EShopController.getStorage().getItem(2));
        assertEquals(2, cart.getItemsCount());

        // purchase the cart
        assertThrows(NoItemInStorage.class, () -> EShopController.purchaseShoppingCart(cart, "Marek", "Hlavna 1"));
        assertEquals(0, EShopController.getPurchasesArchive().getHowManyTimesHasBeenItemSold(EShopController.getStorage().getItem(2)));

        // remove item from the cart
        cart.removeItem(2);
        assertEquals(1, cart.getItemsCount());

        // purchase the cart
        EShopController.purchaseShoppingCart(cart, "Marek", "Hlavna 1");
        assertEquals(1, EShopController.getPurchasesArchive().getHowManyTimesHasBeenItemSold(EShopController.getStorage().getItem(2)));
        assertEquals(0, EShopController.getStorage().getItemCount(2));
    }

    @Test
    void twoPurchases_SameItem_Process() throws NoItemInStorage {
        // create new cart
        ShoppingCart cart = EShopController.newCart();
        assertTrue(cart.getCartItems().isEmpty());

        // add item to the cart
        cart.addItem(EShopController.getStorage().getItem(1));
        assertEquals(1, cart.getItemsCount());
        assertEquals("Dancing Panda v.2", cart.getCartItems().get(0).getName());

        // purchase the cart
        EShopController.purchaseShoppingCart(cart, "Marek", "Hlavna 1");
        assertEquals(1, EShopController.getPurchasesArchive().getHowManyTimesHasBeenItemSold(EShopController.getStorage().getItem(1)));
        assertEquals(9, EShopController.getStorage().getItemCount(1));

        // create new cart
        ShoppingCart cart2 = EShopController.newCart();
        assertTrue(cart2.getCartItems().isEmpty());

        // add item to the cart
        cart2.addItem(EShopController.getStorage().getItem(1));
        assertEquals(1, cart2.getItemsCount());

        // purchase the cart
        EShopController.purchaseShoppingCart(cart2, "Marek", "Hlavna 1");
        assertEquals(2, EShopController.getPurchasesArchive().getHowManyTimesHasBeenItemSold(EShopController.getStorage().getItem(1)));
        assertEquals(8, EShopController.getStorage().getItemCount(1));
    }

}
