package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PurchasesArchiveTest {
    private HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive = new HashMap<>();
    {
        itemPurchaseArchive.put(1, new ItemPurchaseArchiveEntry(new StandardItem(1, "item1", 10, "category1", 1)));
        itemPurchaseArchive.put(2, new ItemPurchaseArchiveEntry(new StandardItem(2, "item2", 20, "category2", 2)));
    }
    private ArrayList<Order> orderArchive;
    {
        orderArchive = new ArrayList<>();
        ShoppingCart cart1 = new ShoppingCart();
        cart1.addItem(new StandardItem(1, "item1", 10, "category1", 1));
        orderArchive.add(new Order(cart1, "Marek", "Hlavna 1", 1));
        ShoppingCart cart2 = new ShoppingCart();
        cart2.addItem(new StandardItem(2, "item2", 20, "category2", 2));
        orderArchive.add(new Order(cart2, "Lukas", "Cervena 2", 3));;
    }
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;
    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }
    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    // Difference only in line separators
    @Test
    void printItemPurchaseStatisticsTest() {
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchive, orderArchive);
        purchasesArchive.printItemPurchaseStatistics();
        String expectedOutput = "ITEM PURCHASE STATISTICS:" + System.lineSeparator() +
                "ITEM  Item   ID 1   NAME item1   CATEGORY category1   PRICE 10.0   LOYALTY POINTS 1   HAS BEEN SOLD 1 TIMES" + System.lineSeparator() +
                "ITEM  Item   ID 2   NAME item2   CATEGORY category2   PRICE 20.0   LOYALTY POINTS 2   HAS BEEN SOLD 1 TIMES" + System.lineSeparator();
        assertEquals(expectedOutput, outContent.toString());
    }


    @Test
    void getHowManyTimesHasBeenItemSold_Equals1() {
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchive, orderArchive);
        assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(new StandardItem(1, "item1", 10, "category1", 1)));
    }

    @Test
    void putOrderToPurchasesArchive_PreexistingOrderArchive_OrderArchiveSizeEquals3() {
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemPurchaseArchive, orderArchive);
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(3, "item3", 30, "category3", 3));
        purchasesArchive.putOrderToPurchasesArchive(new Order(cart, "Marek", "Hlavna 1", 1));
        assertTrue(purchasesArchive.orderArchive.size() == 3);
    }

    @Test
    void putOrderToPurchasesArchive_NewOrderArchive_OrderArchiveSizeEquals1() {
        PurchasesArchive purchasesArchive = new PurchasesArchive();
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(3, "item3", 30, "category3", 3));
        purchasesArchive.putOrderToPurchasesArchive(new Order(cart, "Marek", "Hlavna 1", 1));
        assertTrue(purchasesArchive.orderArchive.size() == 1);
    }

    @Test
    void putOrderToPurchasesArchiveTest_OrderGetItemsCalled() {
        PurchasesArchive purchasesArchive = new PurchasesArchive();
        Order mockedOrder = mock(Order.class);
        purchasesArchive.putOrderToPurchasesArchive(mockedOrder);
        verify(mockedOrder).getItems();
    }


}
