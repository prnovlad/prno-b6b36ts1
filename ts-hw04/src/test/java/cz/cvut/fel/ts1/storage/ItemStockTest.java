package cz.cvut.fel.ts1.storage;

import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {

    private final StandardItem item = new StandardItem(1, "name", 1.0f, "category", 1);
    @Test
    void constructorTest() {
        ItemStock itemStock = new ItemStock(item);
        assertEquals(itemStock.getItem(), item);
        assertEquals(itemStock.getCount(), 0);
    }

    @Test
    void constructorTest_nullItem() {
        assertThrows(NullPointerException.class, () -> new ItemStock(null));
    }

    @ParameterizedTest(name="IncreaseItemCountTestBy{0}_getCount{0}")
    @ValueSource(ints = {1, 2, 3, 4, 5})
    void increaseItemCountTestByX_getCountX(int x) {
        ItemStock itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(x);
        assertEquals(itemStock.getCount(), x);
    }

    void increaseItemCountTestByX_negativeX() {
        ItemStock itemStock = new ItemStock(item);
        assertThrows(IllegalArgumentException.class, () -> itemStock.IncreaseItemCount(-1));
    }


}
