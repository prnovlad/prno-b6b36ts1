package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;


class StandardItemTest {

    @Test
    void constructorTest_matchingProperties() {
        StandardItem item = new StandardItem(1, "name", 1.0f, "category", 1);
        assertEquals(1, item.getID());
        assertEquals("name", item.getName());
        assertEquals(1.0f, item.getPrice());
        assertEquals("category", item.getCategory());
        assertEquals(1, item.getLoyaltyPoints());
    }

    @Test
    void constructorTest_nullName() {
        assertThrows(NullPointerException.class, () -> new StandardItem(1, null, 1.0f, "category", 1));
    }

    @Test
    void constructorTest_nullCategory() {
        assertThrows(NullPointerException.class, () -> new StandardItem(1, "name", 1.0f, null, 1));
    }

    @Test
    void constructorTest_negativePrice() {
        assertThrows(IllegalArgumentException.class, () -> new StandardItem(1, "name", -1.0f, "category", 1));
    }

    @Test
    void constructorTest_negativeLoyaltyPoints() {
        assertThrows(IllegalArgumentException.class, () -> new StandardItem(1, "name", 1.0f, "category", -1));
    }

    @Test
    void constructorTest_zeroPrice() {
        assertThrows(IllegalArgumentException.class, () -> new StandardItem(1, "name", 0.0f, "category", 1));
    }


    @Test
    void copyTest_equalsInitialObject() {
        StandardItem item = new StandardItem(1, "name", 1.0f, "category", 1);
        StandardItem copy = item.copy();
        assertEquals(item, copy);
    }

    @ParameterizedTest(name="equalsTest_matchingObjects")
    @CsvSource({
            "1, name, 1.0, category, 1, 1, name, 1.0, category, 1",
            "2, name2, 2.0, category2, 2, 2, name2, 2.0, category2, 2",
            "3, name3, 3.0, category3, 3, 3, name3, 3.0, category3, 3"
    })
    void equalsTest_matchingObjects(int id1, String name1, float price1, String category1, int loyaltyPoints1,
                                    int id2, String name2, float price2, String category2, int loyaltyPoints2) {
        StandardItem item1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem item2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);
        assertEquals(item1, item2);
    }
}
