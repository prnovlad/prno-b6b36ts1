package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {

    private ShoppingCart cart;
    @BeforeEach
    void setUp() {
        cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Item1", 1, "Category1", 1));
    }

    @Test
    void constructorWithStateTest() {

        Order order = new Order(cart, "Marek", "Hlavna 1", 1);
        assertEquals("Marek", order.getCustomerName());
        assertEquals("Hlavna 1", order.getCustomerAddress());
        assertEquals(1, order.getState());
    }

    @Test
    void constructorWithoutStateTest() {
        Order order = new Order(cart, "Marek", "Hlavna 1");
        assertEquals("Marek", order.getCustomerName());
        assertEquals("Hlavna 1", order.getCustomerAddress());
        assertEquals(0, order.getState());
    }

    @Test
    void constructorNullCustomerNameTest() {
        assertThrows(NullPointerException.class, () -> new Order(cart, null, "Hlavna 1"));
    }

    @Test
    void constructorNullCustomerAddressTest() {
        assertThrows(NullPointerException.class, () -> new Order(cart, "Marek", null));
    }

    @Test
    void constructorNullCustomerNameAndAddressTest() {
        assertThrows(NullPointerException.class, () -> new Order(cart, null, null));
    }

    @Test
    void constructorNullCartTest() {
        assertThrows(NullPointerException.class, () -> new Order(null, "Marek", "Hlavna 1"));
    }

    @Test
    void constructorEmptyCartTest() {
        ShoppingCart cart = new ShoppingCart();
        assertThrows(IllegalArgumentException.class, () -> new Order(cart, "Marek", "Hlavna 1"));
    }

    @Test
    void constructorNegativeStateTest() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "Item1", 1, "Category1", 1));
        assertThrows(IllegalArgumentException.class, () -> new Order(cart, "Marek", "Hlavna 1", -1));
    }

}
