package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

public class PurchasesArchiveMockTest {
    @Mock
    public PurchasesArchive mockedPurchasesArchive;

    @Test
    void printItemPurchaseStatistics_CallOnce() {
        PurchasesArchive mockedPurchasesArchive = mock(PurchasesArchive.class);
        mockedPurchasesArchive.printItemPurchaseStatistics();
        verify(mockedPurchasesArchive, times(1)).printItemPurchaseStatistics();
    }

    @Test
    void getHowManyTimesHasBeenItemSold_CallOnce() {
        PurchasesArchive mockedPurchasesArchive = mock(PurchasesArchive.class);
        StandardItem item = new StandardItem(1, "item1", 10, "category1", 1);
        when(mockedPurchasesArchive.getHowManyTimesHasBeenItemSold(item)).thenReturn(1);
        mockedPurchasesArchive.getHowManyTimesHasBeenItemSold(item);
        verify(mockedPurchasesArchive, times(1)).getHowManyTimesHasBeenItemSold(item);
    }

    @Test
    void putOrderToPurchasesArchive_CallOnce() {
        PurchasesArchive mockedPurchasesArchive = mock(PurchasesArchive.class);
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "item1", 10, "category1", 1));
        Order order = new Order(cart, "Marek", "Hlavna 1", 1);
        mockedPurchasesArchive.putOrderToPurchasesArchive(order);
        verify(mockedPurchasesArchive, times(1)).putOrderToPurchasesArchive(order);
    }
}
