package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ItemPurchaseArchiveEntryMockTest {

    @Mock
    public ItemPurchaseArchiveEntry mockedItemPurchaseArchiveEntry;

    @Test
    void increaseCountHowManyTimesHasBeenSold_CallOnce() {
        ItemPurchaseArchiveEntry mockedItemPurchaseArchiveEntry = mock(ItemPurchaseArchiveEntry.class);
        mockedItemPurchaseArchiveEntry.increaseCountHowManyTimesHasBeenSold(2);
        verify(mockedItemPurchaseArchiveEntry, times(1)).increaseCountHowManyTimesHasBeenSold(2);
    }

    @Test
    void getCountHowManyTimesHasBeenSold_Equals1() {
        ItemPurchaseArchiveEntry mockedItemPurchaseArchiveEntry = mock(ItemPurchaseArchiveEntry.class);
        when(mockedItemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold()).thenReturn(1);
        assertEquals(1, mockedItemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());
    }

    @Test
    void getRefItem_EqualsItem() {
        ItemPurchaseArchiveEntry mockedItemPurchaseArchiveEntry = mock(ItemPurchaseArchiveEntry.class);
        Item item = new StandardItem(1, "item1", 10, "category1", 1);
        when(mockedItemPurchaseArchiveEntry.getRefItem()).thenReturn(item);
        assertEquals(item, mockedItemPurchaseArchiveEntry.getRefItem());
    }

}
