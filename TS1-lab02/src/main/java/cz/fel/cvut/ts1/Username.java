package cz.fel.cvut.ts1;

public class Username {
    public static int factorialIterative(int n){
        int result=1,i=1;

        while(i<=n){
            result=result*i;
            i++;
        }

        return result;
    }

    public static int factorialRecursive(int n){
        if(n <= 1){
            return 1;
        }
        else{
            return n*factorialRecursive(n-1);
        }
    }

    public static int power(int number, int power){
        int result = 1;
        for(int i = 0; i < power; i++){
            result *= number;
        }
        return result;
    }
}
