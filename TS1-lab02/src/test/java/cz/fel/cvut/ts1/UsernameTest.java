package cz.fel.cvut.ts1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class UsernameTest {
    @Test
    void factorialIterativeTest() {
        Username username = new Username();
        int inputValue = 3;
        int expectedResult = 6;
        int result = Username.factorialIterative(inputValue);
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void factorialRecursiveTest() {
        Username username = new Username();
        int inputValue = 3;
        int expectedResult = 6;
        int result = Username.factorialRecursive(inputValue);
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void powerTest() {
        Username username = new Username();
        int inputValue = 2;
        int inputPower = 3;
        int expectedResult = 8;
        int result = Username.power(inputValue, inputPower);
        Assertions.assertEquals(expectedResult, result);
    }
}

