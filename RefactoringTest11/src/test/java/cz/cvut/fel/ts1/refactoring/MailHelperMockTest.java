package cz.cvut.fel.ts1.refactoring;

import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

public class MailHelperMockTest {

    @Test
    public void sendMail_saveMail_callOnce()
    {
        DBManager mockedDbManager = mock(DBManager.class);
        MailHelper mailHelper = new MailHelper(mockedDbManager);
        int sendMailIndex = 1;

        when(mockedDbManager.findMail(sendMailIndex)).thenReturn(null);
        mailHelper.sendMail(sendMailIndex);
        verify(mockedDbManager,times(1)).findMail(sendMailIndex);

    }

    @Test
    public void saveMail_callOnce() {
        DBManager mockedDbManager = mock(DBManager.class);
        MailHelper mailHelper = new MailHelper(mockedDbManager);
        Mail mail = new Mail();

        mailHelper.insertMailToDb(mail);
        verify(mockedDbManager, times(1)).saveMail(mail);
    }

    @Test
    public void sendMailAsync_callOnce() {
        DBManager mockedDbManager = mock(DBManager.class);
        MailHelper mailHelper = new MailHelper(mockedDbManager);
        int mailId = 1;

        mailHelper.sendMailAsync(mailId);
        verify(mockedDbManager, times(1)).findMail(mailId);
    }

}
