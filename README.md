# Prno-B6B36TS1

## Popis
Repo na úlohy z TS1 a práca na hodinách.

## Úlohy
1. FactorialTest, PowerTest - in ts1-lab02 folder
2. CalculatorTest
3. Bonus Mock Tests - in RefactoringTest11, bonus in lab
4. ts-hw04 - unit and proccess tests for EShop
5. cv06 - pairwise - pair testing
6. Oxygen Bonus Graphs - in lab07-process-test-bonus, bonus in lab