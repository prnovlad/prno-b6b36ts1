package cz.cvut.fel.ts1;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CalculatorTest {
    @Test
    void testAddition() {
        Calculator calculator = new Calculator();
        double num1 = 1.23;
        double num2 = 4.56;
        String expectedResult = String.valueOf(5.79);
        String result = calculator.addition(num1, num2);
        Assertions.assertEquals(expectedResult, result);
    }
    @Test
    void testSubtraction() {
        Calculator calculator = new Calculator();
        double num1 = 4.56;
        double num2 = 1.23;
        String expectedResult = String.valueOf(3.33);
        String result = calculator.subtraction(num1, num2);
        Assertions.assertEquals(expectedResult, result);
    }
    @Test
    void testMultiplication() {
        Calculator calculator = new Calculator();
        double num1 = 1.22;
        double num2 = 2;
        String expectedResult = String.valueOf(2.44);
        String result = calculator.multiplication(num1, num2);
        Assertions.assertEquals(expectedResult, result);
    }
    @Test
    void testDivision() {
        Calculator calculator = new Calculator();
        double num1 = 5.10;
        double num2 = 2;
        String expectedResult = String.valueOf(2.55);
        String result = calculator.division(num1, num2);
        Assertions.assertEquals(expectedResult, result);
    }
    @Test
    void testDivisionByZero() {
        Calculator calculator = new Calculator();
        double num1 = 4.56;
        double num2 = 0;
        Assertions.assertThrows(IllegalArgumentException.class, () -> calculator.division(num1, num2));
    }
}
