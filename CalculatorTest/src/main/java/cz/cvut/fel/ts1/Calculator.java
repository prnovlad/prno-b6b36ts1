package cz.cvut.fel.ts1;

public class Calculator {
    // Simple calculator that prints result with 2 decimals
    public String addition(double num1, double num2) {
        double result = num1 + num2;
        return String.format("%.2f", result);
    }

    public String subtraction(double num1, double num2) {
        double result = num1 - num2;
        return String.format("%.2f", result);
    }

    public String multiplication(double num1, double num2) {
        double result = num1 * num2;
        return String.format("%.2f", result);
    }

    public String division(double num1, double num2) {
        if (num2 != 0) {
            double result = num1 / num2;
            return String.format("%.2f", result);
        } else {
            throw new IllegalArgumentException("Division by zero is not allowed.");
        }
    }
}